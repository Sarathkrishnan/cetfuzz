#!/usr/bin/env python3
import argparse
import sys
import os
import shutil
import subprocess
import time
import hashlib
import random
import time
import re
import pandas as pd
import pickle
from feature_extractor import process_c_file

VERIFIER32 = "verifier_codes/v32f.o"
VERIFIER64 = "verifier_codes/v64f.o"
VERIFIER32XML = "verifier_codes/v32xml.o"
VERIFIER64XML = "verifier_codes/v64xml.o"

RUNTIME = 900
XMLTIME = 90


def parseCMDArgs():
    parser = argparse.ArgumentParser(
        description="Automatic Test-Suite Generation tool by College of Engineering Trivandrum Kerala"
    )
    parser.add_argument("cfile", nargs="+", help="Absolute Path of C file")
    parser.add_argument(
        "--propertyFile", "-p", required=True, help=".prp file of test specificaton"
    )
    parser.add_argument("--bit", required=False, help="32 or 64")
    parser.add_argument("--version", action="version", version="1.0.0")
    CMDargs = parser.parse_args()

    # coverting abs file path into actual file path
    global cfile, propertyFile, bit
    cfile = os.path.abspath(CMDargs.cfile[0])
    propertyFile = os.path.abspath(CMDargs.propertyFile)
    bit = CMDargs.bit

    # print("cfile : {}".format(cfile))
    # print("Property file : {}".format(propertyFile))
    # print("Architexture : {}".format(bit))

    # Check whether these files actually existing in OS Directory
    if (
        os.path.isfile(cfile)
        and os.path.isfile(propertyFile)
        and ((cfile[-2:] == ".c") or (cfile[-2:] == ".i"))
        and propertyFile[-4:] == ".prp"
    ):
        if bit == None:
            bit == "32"
        if bit != "64":
            if bit != "32":
                print(bit)
                print("Invalid bit value: {}".format(bit))
                sys.exit(0)
    else:
        print("Files are not argument")
        sys.exit(0)
    global prpContent
    with open(propertyFile, "r") as file:
        prpContent = file.read()

    print("\u2714 COMMAND LINE ARGUMENT PARSED")


def check128():
    print("\u2714 LOOKING FOR INT128")
    with open(cfile, "r") as file:
        # Read the contents of the file
        file_contents = file.read()
        # print(file_contents)
        # Check if the desired strings are in the file
        if (
            ("VERIFIER_nondet_int128" in file_contents)
            or ("__VERIFIER_nondet_uint128" in file_contents)
        ) and bit == "32":
            print(
                "NOT_SUPPORTED : int128 and uint128 is not supported in 32bit architecture"
            )
            sys.exit(0)  # Exit the program with a success code


def copyFiles():
    global temp_dir_path
    home_dir = os.path.expanduser("~")  # Get the user's home directory
    directory_name = "temp"
    temp_dir_path = os.path.join(home_dir, directory_name)

    # Check if the directory already exists at the specified location
    if os.path.exists(temp_dir_path):
        # If it exists, remove it and its contents
        shutil.rmtree(temp_dir_path)

    # Create the directory at the specified location
    os.mkdir(temp_dir_path)

    # Copy the files to the "temp" directory
    shutil.copy(cfile, temp_dir_path)
    shutil.copy(propertyFile, temp_dir_path)

    print("\u2714 FILE COPIED TO TEMP DIRECTORY")


def remove_lines(file_path):
    global temp_dir_path, c_file_for_extraction

    # removing header file benchmark program save  as 'extract.c'
    with open(file_path, "r") as file:
        content = file.read()
    content = re.sub(r"^\s*#.*", "", content, flags=re.MULTILINE)
    c_file_for_extraction = os.path.join(temp_dir_path, "extract.c")
    with open(c_file_for_extraction, "w") as file:
        file.write(content)


def preprocess_c_file():
    global c_file_for_extraction, toolDir
    try:
        base_file_name = os.path.splitext(os.path.basename(c_file_for_extraction))[0]
        output_preprocessed_file_path = os.path.join(
            temp_dir_path, base_file_name + "_preprocessed.c"
        )

        fake_lib_path = os.path.join(toolDir, "fake_libc_include")
        preprocess_command = f"clang-14 -E -I{fake_lib_path} {c_file_for_extraction} -o {output_preprocessed_file_path}"
        subprocess.run(preprocess_command, shell=True, check=True)

        return output_preprocessed_file_path
    except Exception as e:
        print(f"Error preprocessing {c_file_for_extraction}: {e}")
        return "None"


def process_and_predict(preprocessed_file_path):
    global prediction
    try:
        (
            num_lines,
            num_functions,
            cyclomatic_complexity,
            total_edges,
            longest_path,
            loop_depth,
            equality_constraints,
            inequality_constraints,
            max_depth_call,
        ) = process_c_file(preprocessed_file_path)
    except Exception as e:
        # print(f"Error processinsg : {e}")
        num_lines = (
            num_functions
        ) = (
            cyclomatic_complexity
        ) = (
            total_edges
        ) = (
            longest_path
        ) = (
            loop_depth
        ) = equality_constraints = inequality_constraints = max_depth_call = 0

        # print("no lines", num_lines)
        # print("no_functions", num_functions)
        # print("cyclomatic_complexity", cyclomatic_complexity)
        # print("edges_in_AST", total_edges)
        # print("height_AST", longest_path)
        # print("maximum_loop_depth", loop_depth)
        # print("no_equality_constraints", equality_constraints)
        # print("no_inequality_constraints", inequality_constraints)
        # print("no_inequality_constraints", max_depth_call)

    try:
        model_filename = "rf_classifier.pkl"
        with open(model_filename, "rb") as model_file:
            loaded_model = pickle.load(model_file)
        new_data = pd.DataFrame(
            {
                "no_lines": [num_lines],
                "no_functions": [num_functions],
                "cyclomatic_complexity": [cyclomatic_complexity],
                "edges_in_AST": [total_edges],
                "height_AST": [longest_path],
                "maximum_loop_depth": [loop_depth],
                "no_equality_constraints": [equality_constraints],
                "no_inequality_constraints": [inequality_constraints],
                "callstack_depth": [max_depth_call],
            }
        )

        if num_lines == 0:
            prediction = [0]
        else:
            prediction = loaded_model.predict(new_data)

    except Exception as e:
        print(f"Error predicting : {e}")
        prediction = [0]


def extractFeature():
    global cfile
    remove_lines(cfile)
    preprocessed_file_path = preprocess_c_file()
    process_and_predict(preprocessed_file_path)
    print("\u2714 FEATURE EXTRACTION COMPLETED")


def generateSeed():
    seedDir = os.path.join(toolDir, "input/")
    if os.path.exists(seedDir):
        shutil.rmtree(seedDir)
    os.mkdir(seedDir)
    # create a directory if not exist
    random_number = random.randint(10000, 1000000)

    # print("random seed generated is:" , random_number)
    seedFile = os.path.join(seedDir, "seed")
    # Save the byte string to a file named "seed"
    with open(seedFile, "w") as file:
        file.write(str(random_number))

    # print("Random integer saved to 'seed' file.")


def compileCFile():
    global prediction
    # fuzz until crash

    if "AFL_BENCH_UNTIL_CRASH" in os.environ:
        os.environ.pop("AFL_BENCH_UNTIL_CRASH")

    if "AFL_LLVM_INSTRUMENT" in os.environ:
        os.environ.pop("AFL_LLVM_INSTRUMENT")

    if "AFL_LLVM_NGRAM_SIZE" in os.environ:
        os.environ.pop("AFL_LLVM_NGRAM_SIZE")

    if prediction == [2]:
        os.environ["AFL_LLVM_INSTRUMENT"] = "CTX"
    else:
        os.environ["AFL_LLVM_NGRAM_SIZE"] = "10"
    verifier_loc32 = os.path.join(toolDir, VERIFIER32)
    verifier_loc64 = os.path.join(toolDir, VERIFIER64)
    # print("verifier_loc", verifier_loc)

    os.chdir(temp_dir_path)

    global AFL_CLANG
    AFL_CLANG = os.path.join(toolDir, "fuzzer/afl-clang-fast")
    # print(AFL_CLANG)
    if bit == "32":
        instrument = (
            AFL_CLANG
            + " -m32 "
            + cfile
            + " "
            + verifier_loc32
            + " -o mainFuzz > compile_log.txt"
        )

    else:
        instrument = (
            AFL_CLANG
            + " -m64 "
            + cfile
            + " "
            + verifier_loc64
            + " -o mainFuzz > compile_log.txt"
        )
        # print(instrument)
    # print(instrument)
    try:
        # print(temp_dir_path)
        subprocess.run(instrument, shell=True, check=True)
    except subprocess.CalledProcessError as e:
        # print(instrument)
        print(f"INSTRUMENTATION FAILED. CODE: {e.returncode}")
    except FileNotFoundError:
        print("FILE NOT FOUND")
    print("\u2714 BINARY COMPILED")


def fuzzCFile():
    global RUNTIME, XMLTIME
    # open prp file and look for coverage

    # if goal is coverError:
    # Then fuzz until first crash or fuzz upto 12 min

    # if goal is cover branch fuzz for 12 min
    # then fuzz upto 12 min from begin of startTime

    # Define the string to check
    print("\u2714 FUZZING BEGINS")
    os.chdir(temp_dir_path)
    coverError = "COVER EDGES(@CALL(__VERIFIER_error))"
    # coverBranch1 = "COVER EDGES(@DECISIONEDGE)"
    # coverBranch2 = "COVER EDGES(@BASICBLOCKENTRY)"

    # setCrash = "export AFL_BENCH_UNTIL_CRASH=1"
    # unSetCrash = "unset AFL_BENCH_UNTIL_CRASH"

    # rootAcess = "export AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1"
    # try:
    #     subprocess.run(rootAcess, shell=True, check=True)
    # except subprocess.CalledProcessError as e:
    #     print(f"Command failed before fuzzing with error code {e.returncode}")
    # except Exception as e:
    #     print(f"An error occurred: {e}")
    os.environ["AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES"] = "1"
    os.environ["AFL_SKIP_CPUFREQ"] = "1"

    # AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1
    end_time = time.time()
    timeSoFar = start_time - end_time
    FUZZTIME = RUNTIME - int(timeSoFar) - XMLTIME
    # fuzz = "timeout " + str(timeRem) + "s " + "afl-fuzz -t+ -i " + \
    #     toolDir+"/input/ -o output/ ./mainFuzz #> fuzzLog.txt"
    global AFL_FUZZ
    AFL_FUZZ = os.path.join(toolDir, "fuzzer/afl-fuzz")
    # set_env = """export AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES=1
    # export AFL_SKIP_CPUFREQ=1
    #  """
    # AFL_FUZZ = set_env + AFL_FUZZ
    os.environ["AFL_PATH"] = os.path.join(toolDir, "fuzzer")

    global prediction

    if prediction == [1]:
        fuzz = (
            AFL_FUZZ
            + " -p fast -i "
            + toolDir
            + "/input/ -o output/ -V "
            + str(FUZZTIME)
            + " "
            + temp_dir_path
            + "/mainFuzz > fuzzLog.txt"
        )

    elif prediction == [2]:
        fuzz = (
            AFL_FUZZ
            + "-p rare -i "
            + toolDir
            + "/input/ -o output/ -V "
            + str(FUZZTIME)
            + " "
            + temp_dir_path
            + "/mainFuzz > fuzzLog.txt"
        )

    else:
        fuzz = (
            AFL_FUZZ
            + " -p rare -i "
            + toolDir
            + "/input/ -o output/ -V "
            + str(FUZZTIME)
            + " "
            + temp_dir_path
            + "/mainFuzz > fuzzLog.txt"
        )
    # print("Fuzzing command: ", fuzz)

    # Check if the string is in the file content
    if coverError in prpContent:
        try:
            os.environ["FL_BENCH_UNTIL_CRASH"] = "1"
            # subprocess.run(setCrash, shell=True, check=True)
            subprocess.run(fuzz, shell=True, check=True)
        except subprocess.CalledProcessError as e:
            print(f"FUZZING FAILED. CODE:{e.returncode}")
        except Exception as e:
            print(f"ERROR OCCURED: {e}")
    else:
        try:
            # subprocess.run(unSetCrash, shell=True, check=True)
            if "AFL_BENCH_UNTIL_CRASH" in os.environ:
                os.environ.pop("AFL_BENCH_UNTIL_CRASH")
            subprocess.run(fuzz, shell=True, check=True)
        except subprocess.CalledProcessError as e:
            print(f"ERROR CODE {e.returncode}")
        except Exception as e:
            print(f"ERROR CODE {e}")
    print("\u2714 FUZZING COMPLETED")


def convertToXML():
    testSuitDir = os.path.join(toolDir, "test-suite")

    home_dir = os.path.expanduser("~")
    # Check if the "sample" directory exists
    if os.path.exists(testSuitDir):
        shutil.rmtree(testSuitDir)

    # Create a new "sample" directory
    os.mkdir(testSuitDir)
    # shutil.copy(xmlLoc, testSuitDir)

    createMetaData()

    executeC()

    # if cover error, then parse crash folder file

    # if cover branch, then parse the queue folder
    print("\u2714 PREPARING TEST SUIT")


def createMetaData():
    xmlDir = os.path.join(temp_dir_path, "xml")
    if not os.path.exists(xmlDir):
        os.makedirs(xmlDir)
    os.chdir(xmlDir)

    # Get the current time as a Unix timestamp
    current_time_unix = time.time()

    # Get the current local time in a more human-readable format
    current_time = time.strftime(
        "%Y-%m-%dT%H:%M:%S%z", time.localtime(current_time_unix)
    )

    # Open the file in binary mode
    cfile
    with open(cfile, "rb") as file:
        # Read the file contents
        file_contents = file.read()
    # Calculate the SHA-256 hash of the file contents
    sha256_hash = hashlib.sha256(file_contents).hexdigest()

    xmlString = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE test-metadata PUBLIC "+//IDN sosy-lab.org//DTD test-format test-metadata 1.0//EN" "https://sosy-lab.org/test-format/test-metadata-1.0.dtd">
<test-metadata>
       <sourcecodelang>{lang}</sourcecodelang>
       <producer>cetfuzz</producer>
       <specification>{spec}</specification>
       <programfile> {pgmPath}</programfile>
       <programhash> {hash} </programhash>
       <entryfunction>main</entryfunction>
       <architecture>{arch}bit</architecture>
       <creationtime>{time}</creationtime>
</test-metadata>
        """.format(
        lang="C",
        spec=prpContent,
        pgmPath=cfile,
        hash=sha256_hash,
        arch=bit,
        time=current_time,
    )

    # Specify the file name
    file_name = "metadata.xml"

    # Open the file for writing and write the XML string to it
    # this file will be later copied to test-suit directory
    with open(file_name, "w") as file:
        file.write(xmlString)

    # print(f"XML data has been saved to {file_name}")


def executeC():
    # print("inside execC")
    try:
        xmlDir = os.path.join(temp_dir_path, "xml")
        VERIFIER_XML32_LOC = os.path.join(toolDir, VERIFIER32XML)
        VERIFIER_XML64_LOC = os.path.join(toolDir, VERIFIER64XML)

        os.chdir(xmlDir)

        if bit == "32":
            execute1 = (
                "gcc "
                + cfile
                + " "
                + VERIFIER_XML32_LOC
                + " -m32 -o mainExec > xml_parsing_build.txt "
            )
            # print(execute1)
        else:
            execute1 = (
                "gcc "
                + cfile
                + " "
                + VERIFIER_XML64_LOC
                + " -m64 -o mainExec > xml_parsing_build.txt "
            )
        try:
            subprocess.run(execute1, shell=True, check=True)
        except subprocess.CalledProcessError as e:
            print(f"Error: {e}")

        if "@CALL(reach_error)" not in prpContent:
            testDir = os.path.join(temp_dir_path, "output/default/queue")
            # print("cover edge")

        else:
            # print("cover error"+ temp_dir_path)
            testDir = os.path.join(temp_dir_path, "output/default/crashes")
        # Iterate through the files in the queue folder
        # print(os.listdir(testDir))

        if os.listdir(testDir) == []:
            seedLocation = os.path.join(toolDir, "input/seed")
            execute2 = "./mainExec < " + seedLocation + " >> xml_exc_log.txt "
            try:
                result = subprocess.run(
                    execute2,
                    shell=True,
                    check=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
            except Exception as e:
                print(f"An unexpected error occurred: {e}")
        else:
            for filename in os.listdir(testDir):
                # Construct the full path to the file
                if filename == ".state":
                    continue
                if filename == "README.txt":
                    continue
                file_path = os.path.join(testDir, filename)
                # print("file"+file_path)
                # Check if the item is a file (not a directory)
                if os.path.isfile(file_path):
                    # print("c")
                    execute2 = (
                        f"./mainExec < " + file_path + " >> xml_execution_log.txt"
                    )

                    try:
                        os.chdir(xmlDir)
                        # print("current dir:",os.getcwd())

                        result = subprocess.run(
                            execute2,
                            shell=True,
                            # check=True,
                            # stdout=subprocess.PIPE,
                            # stderr=subprocess.PIPE,
                            check=False,
                        )

                    except subprocess.CalledProcessError as e:
                        print(f"ERROR WHILE CREATE TESTCASE XML FILE")

                    except Exception as e:
                        print(f"An unexpected error occurred: {e}")

        # print("last")
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")


def copyXML():
    # Set the source directory and destination directory
    src_dir = os.path.join(temp_dir_path, "xml")
    testSuitDir = os.path.join(toolDir, "test-suite")

    # List all XML files in the source directory
    xml_files = [f for f in os.listdir(src_dir) if f.endswith(".xml")]

    # Copy the remaining XML files to the destination directory
    for xml_file in xml_files:
        src_path = os.path.join(src_dir, xml_file)
        dst_path = os.path.join(testSuitDir, xml_file)
        shutil.copy(src_path, dst_path)

    print("\u2714 TEST_SUIT_CREATED")
    print(prpContent)


def main():
    # enviroment variable for smooth fuzzing
    os.environ["AFL_I_DONT_CARE_ABOUT_MISSING_CRASHES"] = "1"
    os.environ["AFL_SKIP_CPUFREQ"] = "1"

    parseCMDArgs()  # need input 'cfile' and 'PropertyFile' from command line
    check128()  # check if there is a call to verifier_nondet_int128 or verifier_nondet_uint128
    copyFiles()  # create temp directory and load .prp file
    extractFeature()  # extract benchmark program feature and predict fuzzing stratergy
    generateSeed()  # generate an random seed for fuzzing
    compileCFile()  # compile c files against
    fuzzCFile()  # fuzz for a given configuration
    convertToXML()  # convert test case into xml
    copyXML()  # copy xml files into test-suite/ directory


if __name__ == "__main__":
    global start_time, toolDir

    start_time = time.time()
    toolDir = os.getcwd()

    main()
