# CETFUZZER
"cetfuzzer" is coverage driven, automatic test input generation tool. It included AFL++ fuzzer.


##  SYSTEM REQUIREMENTS
1. build-essential
2. clang-14
3. gcc
4. gcc-multilib
5. llvm-runtime
6. lld-14
7. python3
8. python3-sklearn
9. python3-pycparser
10. python3-pandas

## INSTALLATION
Copy the folder in desired location on a system with specified requirements

## THIRDPARTY COMPONENTS
### AFL++ fuzzer
TOOL URL    : https://github.com/AFLplusplus/AFLplusplus

LICENCE     : https://github.com/AFLplusplus/AFLplusplus/blob/stable/LICENSE


### Pycparser
TOOL URL    : https://github.com/eliben/pycparser

LICENCE     : https://github.com/eliben/pycparser/blob/master/LICENSE 

## PACKAGE STRUCTURE
```
.
├── example_test_case.xml
├── fake_libc_include/*
├── feature_extractor.py
├── fuzzer/*
├── LICENSE
├── README.md
├── rf_classifier.pkl
├── runTool.py
└── verifier_codes
    ├── v32f.o
    ├── v32.o
    ├── v32xml.o
    ├── v64f.o
    ├── v64.o
    └── v64xml.o
```
## USAGE
python3 runTool.py <path of .c or .i file> --propertyFile <.prp file location> --bit 64

